init:V:
  openssl dhparam -out data/dhparams.pem 2048

regen:V:
  ./mkpem client
  ./mkpem server
  cat data/dhparams.pem >> server.pem

clean:V:
  rm -f server.key server.crt client.pem client.key .access-token .answer-code .refresh-token

nuke:V:
  rm -f *.pem *.key *.crt data/*.pem
